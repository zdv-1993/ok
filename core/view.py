import json
from django import forms
from django.http import HttpResponse
from django.views.generic import TemplateView, FormView
from allauth.socialaccount.models import SocialToken, SocialApp
from urllib.request import urlopen
import odnoklassniki_wrapper


def get_current_user_ok(application_key, token_secret, token):
    url = 'https://api.ok.ru/fb.do?application_key={application_key}&method=users.getCurrentUser&access_token={token_secret}&format=json&sig={token}'\
        .format(application_key=application_key, token_secret=token_secret, token=token)
    # url = 'https://api.ok.ru/fb.do?application_key={application_key}&method=users.getCurrentUser&access_token={token_secret}&format=json&sig={token}'\

    print(url)

    return url

class OkView(TemplateView):
    template_name='account/ok_profile.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data( **kwargs)
        # acess_token = SocialToken.objects.get(account__user=self.request.user)
        # social_app = SocialApp.objects.first()
        # ok = odnoklassniki_wrapper.Odnoklassniki(social_app.key, social_app.secret, acess_token.token)
        # dd = ok.users.getLoggedInUser()
        # # url = urlopen('https://api.ok.ru/fb.do?application_key=CBANHEFLEBABABABA&method=users.getCurrentUser&access_token=46f918a8a7.1a9a022fe773136a9a70b6b8751a564721a494c26362f09c&sig=4245b81671b02925257eae43cccf279f')
        #
        # context['account_ok'] = dd
        return context
class FormAlbum(forms.Form):
    name = forms.CharField()

class AlbumView(FormView):
    form_class = FormAlbum
    template_name = 'account/ok_album.html'
    def form_valid(self, form):
        pass

def ok_get_user_info_view(request):
    acess_token = SocialToken.objects.get(account__user=request.user)
    social_app = SocialApp.objects.first()
    ok = odnoklassniki_wrapper.Odnoklassniki(social_app.key, social_app.secret, acess_token.token)
    dd = ok.users.getLoggedInUser()
    return HttpResponse()

def ok_other(request):
    acess_token = SocialToken.objects.get(account__user=request.user)
    social_app = SocialApp.objects.first()
    ok = odnoklassniki_wrapper.Odnoklassniki(social_app.key, social_app.secret, acess_token.token)
    dd = ok.friends.getAppUsers()
    return HttpResponse()