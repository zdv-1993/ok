"""src URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import RedirectView, TemplateView
from core.view import OkView, AlbumView, ok_get_user_info_view, ok_other

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^$', RedirectView.as_view(url='/accounts/login/')),
     url(r"^ok/profile/$", OkView.as_view(), name="ok_profile"),
     url(r"^ok/add/$", AlbumView.as_view(), name="ok_album"),
     url(r"^ok/user_info/$", ok_get_user_info_view, name="1"),
     url(r"^ok/ok_2/$", ok_other, name="2"),
     url(r"^ok/ok_3/$", ok_other, name="3"),
     url(r"^ok/ok_4/$", ok_other, name="4"),
     url(r"^ok/ok_5/$", ok_other, name="5"),
]
